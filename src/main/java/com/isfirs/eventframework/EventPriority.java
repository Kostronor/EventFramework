package com.isfirs.eventframework;

/**
 * 
 * @author isfirs
 * @since 1.0.0-a
 */
public enum EventPriority {

	HIGHEST, HIGH, NORMAL, LOW, LOWEST;

}
