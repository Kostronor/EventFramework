package com.isfirs.eventframework;

/**
 * 
 * @author isfirs
 * @since 1.0.0-a
 */
public interface EventManager extends Runnable {
	
	public void registerHandler(Class<?> handler);
	
	public void fireEvent(Event event);
	
	public void stopEventThread( );
	
}