package com.isfirs.eventframework;

/**
 * The basic event type.
 * 
 * @author Isfirs
 * @since 1.0.0-a
 */
public abstract class Event {
	
	/**
	 * The name of this event class
	 */
	private String name;
	
	/**
	 * Is this event asynchronous?
	 */
	private final boolean async;
	
	/**
	 * Constructor.
	 * 
	 * Used for synchronous events.
	 */
	public Event( ) {
		this(false);
	}
	
	/**
	 * Constructor.
	 * 
	 * Used to set the synchronous/asynchronous type of this event.
	 * 
	 * @param isAsync Whether or not this event is asynchronous.
	 */
	public Event(boolean isAsync) {
		this.async = isAsync;
	}
	
	/**
	 * Getter for {@link Event#name}.
	 * 
	 * At first call, sets the value to the instance class simplename.
	 *
	 * @return The name of this event
	 */
	public String getEventName( ) {
		if ( name == null ) {
			name = getClass( ).getSimpleName( );
		}
		
		return name;
	}
	
	/**
	 * Getter for {@link Event#async}.
	 * 
	 * A synchronous event will worked immidiatly when fired. This happens on the same thread on which it gets fired.
	 * As a side effect, thsi thread will block, until th event is processed. This behaviour is wanted, when the result is needed in further processing.
	 * 
	 * Asynchronous events will get stored and executed in the future. This is wanted, when the result is not needed.
	 *
	 * @return false by default, true if the event fires asynchronously
	 */
	public final boolean isAsynchronous( ) {
		return async;
	}
	
}
