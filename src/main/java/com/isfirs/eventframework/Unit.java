package com.isfirs.eventframework;

/**
 * Baseclass with 1 type
 *
 * @author Isfirs
 * @since 1.0
 *
 * @param <A>
 *            {@link Unit#a}
 */
public class Unit<A>
{

    /**
     * Representator A
     */
    public final A a;

    /**
     * Constructor
     *
     * @param a {@link Unit#a}
     */
    public Unit(final A a)
    {
        this.a = a;
    }

    /**
     * Returns representator A in a classic getter way
     *
     * @return {@link Unit#a}
     */
    public A getA()
    {
        return this.a;
    }
}
