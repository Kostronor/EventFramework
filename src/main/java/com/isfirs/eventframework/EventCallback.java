package com.isfirs.eventframework;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class EventCallback
{

    public final Object handler;
    public final Method callback;

    public EventCallback(Object handler, Method callback){

        this.handler = handler;
        this.callback = callback;
    }

    public void handle(final Event event)
    {
        try
        {
            callback.invoke(handler,event);
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }
        catch (InvocationTargetException e)
        {
            e.printStackTrace();
        }
    }
}
