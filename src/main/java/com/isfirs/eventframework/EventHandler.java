package com.isfirs.eventframework;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @author isfirs
 * @since 1.0.0-a
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EventHandler {
	EventPriority priority() default EventPriority.NORMAL;
	
	int level() default 5;
}
