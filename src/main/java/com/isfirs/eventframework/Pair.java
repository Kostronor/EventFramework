package com.isfirs.eventframework;

import com.isfirs.eventframework.Unit;

/**
 * Extended from {@link Unit}.
 *
 * A set of 2 values
 *
 * @author Isfirs
 * @since 1.0
 *
 * @param <A>
 *            {@link Unit#a}
 * @param <B>
 *            {@link Pair#b}
 */
public class Pair<A, B> extends Unit<A>
{

    /**
     * Representator B
     */
    public final B b;

    /**
     * Constructor
     *
     * @param a
     *            {@link Unit#a}
     * @param b
     *            {@link Pair#b}
     */
    public Pair(final A a, final B b) {
        super(a);
        this.b = b;
    }

    /**
     * Returns representator C in a classic getter way
     *
     * @return {@link Pair#b}
     */
    public B getB( ) {
        return this.b;
    }

}