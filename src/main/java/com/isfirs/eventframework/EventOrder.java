package com.isfirs.eventframework;

public class EventOrder implements Comparable<EventOrder>
{

    private static final int HIGH_ORDER = -1000;
    private final EventPriority eventPriority;
    private final int minorPriority;
    public final EventCallback eventCallback;

    public EventOrder(EventPriority eventPriority, int minorPriority, EventCallback eventCallback){
        this.eventPriority = eventPriority;
        this.minorPriority = minorPriority;
        this.eventCallback = eventCallback;
    }


    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     * <p>
     * <p>The implementor must ensure <tt>sgn(x.compareTo(y)) ==
     * -sgn(y.compareTo(x))</tt> for all <tt>x</tt> and <tt>y</tt>.  (This
     * implies that <tt>x.compareTo(y)</tt> must throw an exception iff
     * <tt>y.compareTo(x)</tt> throws an exception.)
     * <p>
     * <p>The implementor must also ensure that the relation is transitive:
     * <tt>(x.compareTo(y)&gt;0 &amp;&amp; y.compareTo(z)&gt;0)</tt> implies
     * <tt>x.compareTo(z)&gt;0</tt>.
     * <p>
     * <p>Finally, the implementor must ensure that <tt>x.compareTo(y)==0</tt>
     * implies that <tt>sgn(x.compareTo(z)) == sgn(y.compareTo(z))</tt>, for
     * all <tt>z</tt>.
     * <p>
     * <p>It is strongly recommended, but <i>not</i> strictly required that
     * <tt>(x.compareTo(y)==0) == (x.equals(y))</tt>.  Generally speaking, any
     * class that implements the <tt>Comparable</tt> interface and violates
     * this condition should clearly indicate this fact.  The recommended
     * language is "Note: this class has a natural ordering that is
     * inconsistent with equals."
     * <p>
     * <p>In the foregoing description, the notation
     * <tt>sgn(</tt><i>expression</i><tt>)</tt> designates the mathematical
     * <i>signum</i> function, which is defined to return one of <tt>-1</tt>,
     * <tt>0</tt>, or <tt>1</tt> according to whether the value of
     * <i>expression</i> is negative, zero or positive.
     *
     * @param o the object to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     * @throws NullPointerException if the specified object is null
     * @throws ClassCastException   if the specified object's type prevents it
     *                              from being compared to this object.
     */
    @Override
    public int compareTo(final EventOrder o)
    {
        switch (o.eventPriority){
            case HIGHEST:
                if(this.eventPriority == EventPriority.HIGHEST){
                    return compareByMinor(o);
                }
                return HIGH_ORDER;
            case HIGH:
                if(this.eventPriority == EventPriority.HIGH){
                    return compareByMinor(o);
                }
                if(this.eventPriority == EventPriority.HIGHEST){
                    return -HIGH_ORDER;
                }
                return HIGH_ORDER;
            case NORMAL:
                if(this.eventPriority == EventPriority.NORMAL){
                    return compareByMinor(o);
                }
                if(this.eventPriority == EventPriority.HIGHEST
                   || this.eventPriority == EventPriority.HIGH){
                    return -HIGH_ORDER;
                }
                return HIGH_ORDER;
            case LOW:
                if(this.eventPriority == EventPriority.LOW){
                    return compareByMinor(o);
                }
                if(this.eventPriority == EventPriority.HIGHEST
                   || this.eventPriority == EventPriority.HIGH
                   || this.eventPriority == EventPriority.NORMAL){
                    return -HIGH_ORDER;
                }
                return HIGH_ORDER;
            case LOWEST:
                if(this.eventPriority == EventPriority.LOWEST){
                    return compareByMinor(o);
                }
                return -HIGH_ORDER;
        }
        return 0;
    }

    private int compareByMinor(final EventOrder o){
        return this.minorPriority - o.minorPriority;
    }
}

