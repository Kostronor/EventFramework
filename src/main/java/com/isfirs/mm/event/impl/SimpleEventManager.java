package com.isfirs.mm.event.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.isfirs.eventframework.*;

/**
 * A simple implementation using a subthread for asynchonous events.
 * 
 * @author Isfirs
 * @since 1.0.0
 */
public class SimpleEventManager<A extends Object> implements EventManager {

	/**
	 * The registered handlers.
	 */
	private final Map<Class<? extends Event>, Map<EventPriority, Map<Integer, List<Pair<Object, Method>>>>> handlerz;

	private final Map<Class<? extends Event>, PriorityQueue<EventOrder>> handlers;

	/**
	 * The logger.
	 */
	private final Logger logger;

	/**
	 * List of stored synchronous events.
	 */
	private final List<Event> pendingSyncEvents;

	/**
	 * List of stored asynchonous events
	 */
	private final List<Event> pendingAsyncEvents;

	/**
	 * The main application.
	 */
	private A main;

	/**
	 * The event thread.
	 */
	private final Thread eventThread;

	/**
	 * Toggle variable, if the thread is requested to stop.
	 */
	private boolean runEventThread;

	/**
	 * Used to gather a lock for certain actions.
	 */
	private final Lock lock;

	/**
	 * Constructor.
	 * 
	 * Used for tests only.
	 */
	@SuppressWarnings("unchecked")
	@Deprecated
	public SimpleEventManager() {
		this.main = (A) new Object();

		// Prepare a simple logger
		this.logger = Logger.getLogger(SimpleEventManager.class.getSimpleName());

		handlerz = new HashMap<Class<? extends Event>, Map<EventPriority, Map<Integer, List<Pair<Object, Method>>>>>();
        handlers = new HashMap<>();

		pendingAsyncEvents = new ArrayList<Event>();
		pendingSyncEvents = new ArrayList<Event>();

		lock = new ReentrantLock();

		eventThread = new Thread(this, "Event-Thread");
		runEventThread = true;
		eventThread.start();

	}

	/**
	 * Constructor.
	 * 
	 * @param main
	 *            The main application
	 */
	public SimpleEventManager(A main) {
		this.main = main;
		this.logger = Logger.getLogger(this.main.getClass().getSimpleName());

		handlerz = new HashMap<Class<? extends Event>, Map<EventPriority, Map<Integer, List<Pair<Object, Method>>>>>();
        handlers = new HashMap<>();

		pendingAsyncEvents = new ArrayList<Event>();
		pendingSyncEvents = new ArrayList<Event>();

		lock = new ReentrantLock();

		eventThread = new Thread(this, "Event-Thread");
		runEventThread = true;
		eventThread.start();
	}

	@Override
	public void registerHandler(Class<?> handler) {
		logger.fine("Attemp to instantiate the handler object");

		Object eventHandler = null;
		try {
			logger.fine(String.format("Getting constructor(%s)", main.getClass().getName()));

			Constructor<?> constructor = null;
			try {
				constructor = handler.getConstructor(main.getClass());

				logger.fine("Creating instance of handler with main reference");
				eventHandler = constructor.newInstance(main);
			} catch (NoSuchMethodException e) {
				logger.info(String.format("Can not find a constructor `%s(%s)`", handler.getName(),
						main.getClass().getName()));
			}

			if (eventHandler == null)
				try {
					logger.info(String.format("Attemp to search default constructor"));

					constructor = handler.getConstructor();

					logger.fine("Creating instance of handler");
					eventHandler = constructor.newInstance();
				} catch (NoSuchMethodException e) {
					logger.log(Level.WARNING,
							String.format("Can not find a default constructor inside `%s`", handler.getName()), e);

					logger.warning(String.format("The handler `%s` is refused", handler.getName()));
					return;
				}

		} catch (SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);

			logger.warning(String.format("The handler `%s` is refused", handler.getName()));
			return;
		}

		logger.fine("Get all methods of handler class");
		Method[] methods = handler.getDeclaredMethods();

		logger.fine("Searching for valid methods");
		for (Method method : methods) {
			logger.fine(
					String.format("Check if method `%s` has annotation `%s`", method.getName(), EventHandler.class));
			if (method.isAnnotationPresent(EventHandler.class)) {
				if (method.getParameterCount() == 1) {
					Class<?>[] parameterTypes = method.getParameterTypes();
					Class<?> parameterType = parameterTypes[0];

					logger.fine(
							String.format("Check if parameter `%s` is subtype of `%s`", parameterType, Event.class));
					if (Event.class.isAssignableFrom(parameterType)) {
						@SuppressWarnings("unchecked")
						Class<? extends Event> parameter = (Class<? extends Event>) parameterType;

						// Process Annotation
						EventHandler eventHandlerAnnotation = method.getAnnotation(EventHandler.class);
						if (!handlerz.containsKey(parameter)) {
							logger.fine("New key type `%s`. Registering new priority map");

							handlerz.put(parameter,
									new HashMap<EventPriority, Map<Integer, List<Pair<Object, Method>>>>());
						}

						if (!handlerz.get(parameter).containsKey(eventHandlerAnnotation.priority())) {
							logger.fine("New new priority `%s` for type `%s`. Registering new level Map");

							handlerz.get(parameter).put(eventHandlerAnnotation.priority(),
									new HashMap<Integer, List<Pair<Object, Method>>>());
						}

						if (!handlerz.get(parameter).get(eventHandlerAnnotation.priority())
								.containsKey(eventHandlerAnnotation.level())) {
							logger.fine("New level for type `%s` on priority `%s`");

							handlerz.get(parameter).get(eventHandlerAnnotation.priority())
									.put(eventHandlerAnnotation.level(), new ArrayList<Pair<Object, Method>>());
						}



						// Register Object:Method for Event
						// .add(new Pair<Object, Method>(handler, method));
						logger.fine("Registering Object:Method");
						List<Pair<Object, Method>> pairs = handlerz.get(parameter)
								.get(eventHandlerAnnotation.priority()).get(eventHandlerAnnotation.level());

						boolean found = false;
						for (Pair<Object, Method> pair : pairs) {
							if (pair.getA().getClass().equals(handler.getClass())) {
								found = true;
							}

						}

						if (found) {

						} else {
                            addEventToHandlers(parameter, new EventOrder(eventHandlerAnnotation.priority(), eventHandlerAnnotation.level(),
                                                                         new EventCallback(eventHandler, method)));
						}

						logger.fine("Successfully registered");
					} else {
						// Parameter is not a subtype of Event.class. Warn and
						// ignore
						logger.warning(String.format("The parameter is not a subtype of `%s`", Event.class));
					}
				} else {
					logger.warning("Method has less or more then 1 parameter");
				}
			} else {
				logger.info(String.format("Method `%s` has no annotation `%s`. Ignored", method.getName(),
						EventHandler.class));
			}
		}
	}

	private void addEventToHandlers(Class<? extends Event> clazz,EventOrder event){
	    if(!handlers.containsKey(clazz)){
	        handlers.put(clazz, new PriorityQueue<EventOrder>());
        }
        handlers.get(clazz).add(event);
    }

	@Override
	public void fireEvent(Event event) {
		logger.fine(String.format("Firing event `%s`", event.getEventName()));

		this.lock.lock();
		if (event.isAsynchronous()) {

			this.pendingAsyncEvents.add(event);

		} else {

			this.pendingSyncEvents.add(event);
			List<Event> list = new ArrayList<Event>(this.pendingSyncEvents);
			this.pendingSyncEvents.clear();
			this.processEvents(list);

		}
		this.lock.unlock();
	}

	@Override
	public void stopEventThread() {
		this.runEventThread = false;
	}

	@Override
	public void run() {
		while (runEventThread || this.pendingAsyncEvents.size() > 0) {
			this.lock.lock();

			if (this.pendingAsyncEvents.size() == 0) {
				this.lock.unlock();

				continue;
			}

			List<Event> list = null;

			list = new ArrayList<Event>(this.pendingAsyncEvents);
			this.pendingAsyncEvents.clear();

			this.processEvents(list);

			this.lock.unlock();
		}
	}

	private void processEvents(List<Event> process) {
		for (Event event : process) {
			for (EventPriority prio : EventPriority.values()) {
				Map<EventPriority, Map<Integer, List<Pair<Object, Method>>>> map = this.handlerz.get(prio);

				if (map == null) {
					// No handlers.

					continue;
				}

			}

            if(handlers.containsKey(event.getClass())){
                final PriorityQueue<EventOrder> queue = handlers.get(event.getClass());
                for(EventOrder e : queue){
                    final EventCallback c = e.eventCallback;
                    c.handle(event);
                }
            }
		}
	}

}
