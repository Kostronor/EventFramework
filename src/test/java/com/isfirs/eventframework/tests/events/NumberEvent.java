package com.isfirs.eventframework.tests.events;

import com.isfirs.eventframework.Event;

/**
 * This is a test event.
 * 
 * It will hold 2 unmodifyable numbers.
 * 
 * @author Isfirs
 */
public class NumberEvent extends Event {
	
	public int first;
	public int second;

	/**
	 * Default constructor
	 */
	public NumberEvent(final int first, final int second) {
		this.first = first;
		this.second = second;
	}

	public int getFirst() {
		return first;
	}

	public int getSecond() {
		return second;
	}
	
}
