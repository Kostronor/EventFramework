package com.isfirs.eventframework.tests;

import static org.junit.Assert.assertEquals;

import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import com.isfirs.eventframework.tests.events.NumberEvent;
import com.isfirs.eventframework.tests.handlers.HighPriorityHandler;
import com.isfirs.eventframework.tests.handlers.LowPriorityHandler;
import com.isfirs.eventframework.tests.handlers.MediumPriorityHandler;
import com.isfirs.eventframework.tests.logger.TestFormat;
import com.isfirs.mm.event.impl.SimpleEventManager;

public class EventPriorityTest {

	private SimpleEventManager<Object> eventManager;
	private Logger logger;

	/**
	 * Setup needed resources
	 */
	@Before
	public void setup() {
		logger = Logger.getLogger(SimpleEventManager.class.getSimpleName());

		Handler cHandler = new ConsoleHandler();
		Formatter format = new TestFormat();
		cHandler.setFormatter(format);
		logger.addHandler(cHandler);

		logger.setLevel(Level.ALL);
		for (Handler handler : logger.getHandlers())
			handler.setLevel(logger.getLevel());

		eventManager = new SimpleEventManager<>();

		eventManager.registerHandler(LowPriorityHandler.class);
		eventManager.registerHandler(MediumPriorityHandler.class);
		eventManager.registerHandler(HighPriorityHandler.class);
	}

	@Test
	public void testPriorityOutcome() {
		NumberEvent event = new NumberEvent(1, 2);
		eventManager.fireEvent(event);

		logger.fine("First is: " + event.getFirst());
		logger.fine("SEcond is: " + event.getSecond());

		assertEquals("First should be 4", 4, event.getFirst());
		assertEquals("Second should be 5", 5, event.getSecond());
	}

}
