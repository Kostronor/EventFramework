package com.isfirs.eventframework.tests.handlers;

import com.isfirs.eventframework.EventHandler;
import com.isfirs.eventframework.EventPriority;
import com.isfirs.eventframework.tests.events.NumberEvent;

public class LowPriorityHandler {

	@EventHandler(priority = EventPriority.LOWEST)
	public void on(NumberEvent event) {
		event.first++;
		event.second++;
	}

}
