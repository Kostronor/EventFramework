package com.isfirs.eventframework.tests.logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class TestFormat extends Formatter {

	public static final String TEMPLATE = "${THREAD} ${DATE} ${CLASS} ${METHOD} - ${MESSAGE}\r\n";

	@Override
	public String format(LogRecord record) {
		String format = TEMPLATE;

		format = format.replace("${THREAD}", Thread.currentThread().getName());
		format = format.replace("${DATE}", new SimpleDateFormat().format(new Date()));
		format = format.replace("${CLASS}", record.getSourceClassName());
		format = format.replace("${METHOD}", record.getSourceMethodName());
		format = format.replace("${MESSAGE}", record.getMessage());

		return format;
	}

}
